Prasoons-MacBook-Pro:AI_Assignment_3 prasoon$ python3 main.py /Users/prasoon/Documents/books/AI/AI_Assignment_3/dataset/Training/ /Users/prasoon/Documents/books/AI/AI_Assignment_3/dataset/Test/ 1 2
[Parsed command line]:
Training data path: /Users/prasoon/Documents/books/AI/AI_Assignment_3/dataset/Training/
Test data path: /Users/prasoon/Documents/books/AI/AI_Assignment_3/dataset/Test/
Config_mode: 1
n_gram baseline mode: 2
Best classification model selected: Logistic Regression
First configuration: TFIDF Vectorizer: Remove stop words + Logistic Regression: L1 regularization
N-gram baseline: Bi-gram

*** Logistic regression ***
F1-score (macro) : 0.542443539863
Classification report:
                        precision    recall  f1-score   support

      rec.sport.hockey       0.76      0.48      0.59       201
               sci.med       0.50      0.42      0.46       199
soc.religion.christian       0.50      0.94      0.66       200
    talk.religion.misc       0.75      0.34      0.47       127

           avg / total       0.62      0.57      0.55       727

Correct: 412, Incorrect: 315
Plotting results: This may take some time, please wait..
Close the plot window manually to continue.. 
Prasoons-MacBook-Pro:AI_Assignment_3 prasoon$ 