from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer, TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier, LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.externals import joblib
import numpy
import sys
import pickle
import os
from collections import defaultdict
from sklearn.metrics import f1_score, classification_report


def display_macro_f1_score(original, predicted):
    print('F1-score (macro) : %s' % f1_score(original, predicted, average='macro'))
    print('Classification report:')
    categories = ['rec.sport.hockey', 'sci.med', 'soc.religion.christian', 'talk.religion.misc']
    print(classification_report(original, predicted, target_names=categories))


def display_results(predicted, twenty_train_target_names, docs_train_filenames):
    match_ct = 0
    not_match_ct = 0
    for doc, category in zip(docs_train_filenames, predicted):
        if doc.split('/')[len(doc.split('/')) - 2] == twenty_train_target_names[category]:
            res = "MATCH"
            match_ct += 1
        else:
            res = "NO_MATCH"
            not_match_ct += 1
            # print('%r=> %s => %r' % (doc.split('/')[len(doc.split('/')) - 2], twenty_train_target_names[category], res))
    print('Correct: %s, Incorrect: %s' % (match_ct, not_match_ct))


def get_predicted_results(count_vect, tfidf_transformer, clf):
    docs_train = load_files(sys.argv[2], encoding='latin1')
    docs_new = docs_train.data
    X_new_counts = count_vect.transform(docs_new)
    X_new_tfidf = tfidf_transformer.transform(X_new_counts)
    predicted = clf.predict(X_new_tfidf)
    return predicted, docs_train, X_new_tfidf


def classifier_naive_bayes(X_train_tfidf, twenty_train, count_vect, tfidf_transformer, plot_dict):
    print("\n*** Naive Baye's ***")
    clf = MultinomialNB().fit(X_train_tfidf, twenty_train.target)
    predicted, docs_train, X_new_tfidf = get_predicted_results(count_vect, tfidf_transformer, clf)
    display_macro_f1_score(docs_train.target, predicted)
    display_results(predicted, twenty_train.target_names, docs_train.filenames)
    model = MultinomialNB()
    plot_dict['MultinomialNB'] = [X_train_tfidf, twenty_train, docs_train, X_new_tfidf, model, 'red']


def classifier_svm(X_train_tfidf, twenty_train, count_vect, tfidf_transformer, plot_dict):
    print("\n*** SVM ***")
    clf = SGDClassifier(loss='hinge').fit(X_train_tfidf, twenty_train.target)
    predicted, docs_train, X_new_tfidf = get_predicted_results(count_vect, tfidf_transformer, clf)
    display_macro_f1_score(docs_train.target, predicted)
    display_results(predicted, twenty_train.target_names, docs_train.filenames)
    model = SGDClassifier()
    plot_dict['SGDClassifier'] = [X_train_tfidf, twenty_train, docs_train, X_new_tfidf, model, 'blue']


def classifier_logistic_regression(X_train_tfidf, twenty_train, count_vect, tfidf_transformer, plot_dict):
    print("\n*** Logistic regression ***")
    clf = LogisticRegression().fit(X_train_tfidf, twenty_train.target)
    predicted, docs_train, X_new_tfidf = get_predicted_results(count_vect, tfidf_transformer, clf)
    display_macro_f1_score(docs_train.target, predicted)
    display_results(predicted, twenty_train.target_names, docs_train.filenames)
    model = LogisticRegression()
    plot_dict['LogisticRegression'] = [X_train_tfidf, twenty_train, docs_train, X_new_tfidf, model, 'green']


def classifier_random_forest(X_train_tfidf, twenty_train, count_vect, tfidf_transformer, plot_dict):
    print("\n*** Random forest ***")
    clf = RandomForestClassifier().fit(X_train_tfidf, twenty_train.target)
    predicted, docs_train, X_new_tfidf = get_predicted_results(count_vect, tfidf_transformer, clf)
    display_macro_f1_score(docs_train.target, predicted)
    display_results(predicted, twenty_train.target_names, docs_train.filenames)
    model = RandomForestClassifier()
    plot_dict['RandomForestClassifier'] = [X_train_tfidf, twenty_train, docs_train, X_new_tfidf, model, 'yellow']


def plot_results(plot_dict, plot_title):
    print("Plotting results: This may take some time, please wait..")
    n_trials = 5
    train_percentages = range(5, 100, 5)
    test_accuracies = numpy.zeros(len(train_percentages))
    plt.title(plot_title)
    plt.grid(True)
    ctr = 4
    for key, value in plot_dict.items():
        for (i, train_percent) in enumerate(train_percentages):
            test_accuracy = numpy.zeros(n_trials)
            for n in range(n_trials):
                X_train, X_test, y_train, y_test = train_test_split(value[0], value[1].target,
                                                                    train_size=train_percent / 100.0)
                value[4].fit(X_train, y_train)

                test_accuracy[n] = value[4].score(value[3], value[2].target)
            test_accuracies[i] = test_accuracy.mean()
        line1, = plt.plot(train_percentages, test_accuracies, '{0}o-'.format(value[5][0]), label=key)
        legend = plt.legend(handles=[line1], loc=ctr)
        ctr -= 1
        _ = plt.gca().add_artist(legend)
    plt.xlabel('Percentage of Data Used for Training')
    plt.ylabel('F1 score on Test Set')
    print("Close the plot window manually to continue.. ")
    plt.show()


def run_default_classification(twenty_train, count_vect):
    plot_dict = defaultdict(list)
    X_train_counts = count_vect.fit_transform(twenty_train.data)
    tfidf_transformer = TfidfTransformer()
    X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
    classifier_naive_bayes(X_train_tfidf, twenty_train, count_vect, tfidf_transformer, plot_dict)
    classifier_svm(X_train_tfidf, twenty_train, count_vect, tfidf_transformer, plot_dict)
    classifier_logistic_regression(X_train_tfidf, twenty_train, count_vect, tfidf_transformer, plot_dict)
    classifier_random_forest(X_train_tfidf, twenty_train, count_vect, tfidf_transformer, plot_dict)
    return plot_dict


def run_MBC_first(twenty_train, n_gram):
    if n_gram == '2':
        print("N-gram baseline: Bi-gram")
        count_vect = TfidfVectorizer(ngram_range=(2, 2), stop_words='english', analyzer='word')
    else:
        print("N-gram baseline: Uni-gram")
        count_vect = TfidfVectorizer(ngram_range=(1, 1), stop_words='english', analyzer='word')
    plot_dict = defaultdict(list)
    X_train_counts = count_vect.fit_transform(twenty_train.data)
    tfidf_transformer = TfidfTransformer()
    X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
    print("\n*** Logistic regression ***")
    clf = LogisticRegression(penalty='l1').fit(X_train_tfidf, twenty_train.target)
    predicted, docs_train, X_new_tfidf = get_predicted_results(count_vect, tfidf_transformer, clf)
    display_macro_f1_score(docs_train.target, predicted)
    display_results(predicted, twenty_train.target_names, docs_train.filenames)
    model = LogisticRegression(penalty='l1')
    plot_dict['LogisticRegression'] = [X_train_tfidf, twenty_train, docs_train, X_new_tfidf, model, 'green']
    return plot_dict


def run_MBC_second(twenty_train, n_gram):
    if n_gram == '2':
        print("N-gram baseline: Bi-gram")
        count_vect = CountVectorizer(ngram_range=(2, 2), stop_words='english', analyzer='word')
    else:
        print("N-gram baseline: Uni-gram")
        count_vect = CountVectorizer(ngram_range=(1, 1), stop_words='english', analyzer='word')
    plot_dict = defaultdict(list)
    X_train_counts = count_vect.fit_transform(twenty_train.data)
    tfidf_transformer = TfidfTransformer()
    X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
    print("\n*** Logistic regression ***")
    clf = LogisticRegression(penalty='l1').fit(X_train_tfidf, twenty_train.target)
    predicted, docs_train, X_new_tfidf = get_predicted_results(count_vect, tfidf_transformer, clf)
    display_macro_f1_score(docs_train.target, predicted)
    display_results(predicted, twenty_train.target_names, docs_train.filenames)
    model = LogisticRegression(penalty='l1')
    plot_dict['LogisticRegression'] = [X_train_tfidf, twenty_train, docs_train, X_new_tfidf, model, 'green']
    return plot_dict


def run_MBC_third(twenty_train, n_gram):
    if n_gram == '2':
        print("N-gram baseline: Bi-gram")
        count_vect = CountVectorizer(stop_words='english', ngram_range=(2, 2), analyzer='word')
    else:
        print("N-gram baseline: Uni-gram")
        count_vect = CountVectorizer(stop_words='english', ngram_range=(1, 1), analyzer='word')
    plot_dict = defaultdict(list)
    X_train_counts = count_vect.fit_transform(twenty_train.data)
    tfidf_transformer = TfidfTransformer()
    X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
    print("\n*** Logistic regression ***")
    clf = LogisticRegression(penalty='l2').fit(X_train_tfidf, twenty_train.target)
    predicted, docs_train, X_new_tfidf = get_predicted_results(count_vect, tfidf_transformer, clf)
    display_macro_f1_score(docs_train.target, predicted)
    display_results(predicted, twenty_train.target_names, docs_train.filenames)
    model = LogisticRegression(penalty='l2')
    plot_dict['LogisticRegression'] = [X_train_tfidf, twenty_train, docs_train, X_new_tfidf, model, 'green']
    return plot_dict


def run_MBC_fourth(twenty_train, n_gram):
    if n_gram == '2':
        print("N-gram baseline: Bi-gram")
        count_vect = CountVectorizer(stop_words='english', ngram_range=(2, 2), analyzer='word')
    else:
        print("N-gram baseline: Uni-gram")
        count_vect = CountVectorizer(stop_words='english', ngram_range=(1, 1), analyzer='word')
    plot_dict = defaultdict(list)
    X_train_counts = count_vect.fit_transform(twenty_train.data)
    tfidf_transformer = TfidfTransformer()
    X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
    print("\n*** Logistic regression ***")
    clf = LogisticRegression(C=91.0, penalty='l2').fit(X_train_tfidf, twenty_train.target)
    predicted, docs_train, X_new_tfidf = get_predicted_results(count_vect, tfidf_transformer, clf)
    display_macro_f1_score(docs_train.target, predicted)
    display_results(predicted, twenty_train.target_names, docs_train.filenames)
    model = LogisticRegression(C=91.0, penalty='l2')
    plot_dict['LogisticRegression'] = [X_train_tfidf, twenty_train, docs_train, X_new_tfidf, model, 'green']
    return plot_dict


def run_MBC_fifth(twenty_train, n_gram):
    if n_gram == '2':
        print("N-gram baseline: Bi-gram")
        count_vect = CountVectorizer(stop_words='english', ngram_range=(2, 2), analyzer='word')
    else:
        print("N-gram baseline: Uni-gram")
        count_vect = CountVectorizer(stop_words='english', ngram_range=(1, 1), analyzer='word')
    plot_dict = defaultdict(list)
    X_train_counts = count_vect.fit_transform(twenty_train.data)
    tfidf_transformer = TfidfTransformer()
    X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
    print("\n*** Logistic regression ***")
    clf = LogisticRegression(C=91.0, max_iter=50, penalty='l2').fit(X_train_tfidf, twenty_train.target)
    predicted, docs_train, X_new_tfidf = get_predicted_results(count_vect, tfidf_transformer, clf)
    display_macro_f1_score(docs_train.target, predicted)
    display_results(predicted, twenty_train.target_names, docs_train.filenames)
    model = LogisticRegression(C=91.0, max_iter=50, penalty='l2')
    plot_dict['LogisticRegression'] = [X_train_tfidf, twenty_train, docs_train, X_new_tfidf, model, 'green']
    return plot_dict


def run_MBC_sixth(twenty_train, n_gram):
    if n_gram == '2':
        print("N-gram baseline: Bi-gram")
        count_vect = CountVectorizer(stop_words='english', ngram_range=(2, 2), binary=True, analyzer='word')
    else:
        print("N-gram baseline: Uni-gram")
        count_vect = CountVectorizer(stop_words='english', ngram_range=(1, 1), binary=True, analyzer='word')
    plot_dict = defaultdict(list)
    X_train_counts = count_vect.fit_transform(twenty_train.data)
    tfidf_transformer = TfidfTransformer()
    X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
    print("\n*** Logistic regression ***")
    clf = LogisticRegression(C=91.0, max_iter=50, penalty='l2').fit(X_train_tfidf, twenty_train.target)
    predicted, docs_train, X_new_tfidf = get_predicted_results(count_vect, tfidf_transformer, clf)
    display_macro_f1_score(docs_train.target, predicted)
    display_results(predicted, twenty_train.target_names, docs_train.filenames)
    model = LogisticRegression(C=91.0, max_iter=50, penalty='l2')
    plot_dict['LogisticRegression'] = [X_train_tfidf, twenty_train, docs_train, X_new_tfidf, model, 'green']
    return plot_dict


def run_MBC_seventh(twenty_train, n_gram):
    if n_gram == '2':
        print("N-gram baseline: Bi-gram")
        count_vect = CountVectorizer(ngram_range=(2, 2), binary=True)
    else:
        print("N-gram baseline: Uni-gram")
        count_vect = CountVectorizer(ngram_range=(1, 1), binary=True)
    plot_dict = defaultdict(list)
    X_train_counts = count_vect.fit_transform(twenty_train.data)
    tfidf_transformer = TfidfTransformer()
    X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
    print("\n*** Logistic regression ***")
    clf = LogisticRegression(C=91.0, max_iter=50, penalty='l2').fit(X_train_tfidf, twenty_train.target)
    predicted, docs_train, X_new_tfidf = get_predicted_results(count_vect, tfidf_transformer, clf)
    display_macro_f1_score(docs_train.target, predicted)
    display_results(predicted, twenty_train.target_names, docs_train.filenames)
    model = LogisticRegression(C=91.0, max_iter=50, penalty='l2')
    plot_dict['LogisticRegression'] = [X_train_tfidf, twenty_train, docs_train, X_new_tfidf, model, 'green']
    return plot_dict


def run_MBC_eighth(twenty_train, n_gram, train_dump, train_flag):
    if n_gram == '2':
        print("N-gram baseline: Bi-gram")
        count_vect = CountVectorizer(ngram_range=(2, 2), binary=True)
    else:
        print("N-gram baseline: Uni-gram")
        count_vect = CountVectorizer(ngram_range=(1, 1), binary=True)
    plot_dict = defaultdict(list)
    X_train_counts = count_vect.fit_transform(twenty_train.data)
    tfidf_transformer = TfidfTransformer()
    X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
    print("\n*** Logistic regression ***")
    clf = LogisticRegression(C=91.0, class_weight='balanced', max_iter=50,
                             penalty='l2').fit(X_train_tfidf, twenty_train.target)
    if train_flag is True:
        joblib.dump(clf, os.path.join(train_dump, 'clf.pkl'))
        joblib.dump(count_vect, os.path.join(train_dump, 'count_vect.pkl'))
        joblib.dump(tfidf_transformer, os.path.join(train_dump, 'tfidf_transformer.pkl'))
        joblib.dump(X_train_tfidf, os.path.join(train_dump, 'X_train_tfidf.pkl'))
        joblib.dump(twenty_train, os.path.join(train_dump, 'twenty_train.pkl'))
        print("Dump the clf vector to pkl file at: {0}".format(os.path.join(train_dump, 'clf.pkl')))
        print("Dump the count_vect vector to pkl file at: {0}".format(os.path.join(train_dump, 'count_vect.pkl')))
        print("Dump the tfidf_transformer vector to pkl file at: {0}".format(os.path.join(train_dump,
                                                                                          'tfidf_transformer.pkl')))
        print("Dump the X_train_tfidf vector to pkl file at: {0}".format(os.path.join(train_dump,
                                                                                          'X_train_tfidf.pkl')))
        print("Dump the twenty_train data to pkl file at: {0}".format(os.path.join(train_dump,
                                                                                          'twenty_train.pkl')))
        return
    predicted, docs_train, X_new_tfidf = get_predicted_results(count_vect, tfidf_transformer, clf)
    display_macro_f1_score(docs_train.target, predicted)
    display_results(predicted, twenty_train.target_names, docs_train.filenames)
    model = LogisticRegression(C=91.0, class_weight='balanced', max_iter=50, penalty='l2', solver='lbfgs')
    plot_dict['LogisticRegression'] = [X_train_tfidf, twenty_train, docs_train, X_new_tfidf, model, 'green']
    return plot_dict


def test_best_configuration(pkl_path, test_data_path):
    plot_dict = defaultdict(list)
    count_vect = joblib.load(os.path.join(pkl_path, 'count_vect.pkl'))
    tfidf_transformer = joblib.load(os.path.join(pkl_path, 'tfidf_transformer.pkl'))
    clf = joblib.load(os.path.join(pkl_path, 'clf.pkl'))
    X_train_tfidf = joblib.load(os.path.join(pkl_path, 'X_train_tfidf.pkl'))
    twenty_train = joblib.load(os.path.join(pkl_path, 'twenty_train.pkl'))
    print("Load the clf vector from pkl file at: {0}".format(os.path.join(pkl_path, 'clf.pkl')))
    print("Load the count_vect vector from pkl file at: {0}".format(os.path.join(pkl_path, 'count_vect.pkl')))
    print("Load the tfidf_transformer vector from pkl file at: {0}".format(
        os.path.join(pkl_path, 'tfidf_transformer.pkl')))
    print("Load the X_train_tfidf vector from pkl file at: {0}".format(
        os.path.join(pkl_path, 'X_train_tfidf.pkl')))
    print("Load the twenty_train data from pkl file at: {0}".format(
        os.path.join(pkl_path, 'twenty_train.pkl')))
    docs_train = load_files(test_data_path, encoding='latin1')
    docs_new = docs_train.data
    X_new_counts = count_vect.transform(docs_new)
    X_new_tfidf = tfidf_transformer.transform(X_new_counts)
    predicted = clf.predict(X_new_tfidf)
    display_macro_f1_score(docs_train.target, predicted)
    model = LogisticRegression(C=91.0, class_weight='balanced', max_iter=50, penalty='l2', solver='lbfgs')
    plot_dict['LogisticRegression'] = [X_train_tfidf, twenty_train, docs_train, X_new_tfidf, model, 'green']
    plot_title = "Eighth (best) configuration: Plot of F-1 score vs training data percentage"
    plot_results(plot_dict, plot_title)


def my_best_configuration(twenty_train, mode, n_gram):
    print("Best classification model selected: Logistic Regression")
    plot_dict = defaultdict(list)
    plot_title = ''
    if mode == '1':
        print("First configuration: TFIDF Vectorizer: Remove stop words + Logistic Regression: L1 regularization")
        plot_dict = run_MBC_first(twenty_train, n_gram)
        plot_title = "First configuration: Plot of F-1 score vs training data percentage"

    if mode == '2':
        print("Second configuration: Count Vectorizer: Remove stop words + Logistic Regression: L1 regularization")
        plot_dict = run_MBC_second(twenty_train, n_gram)
        plot_title = "Second configuration: Plot of F-1 score vs training data percentage"

    if mode == '3':
        print("Third configuration: Count Vectorizer: Remove stop words + Logistic Regression: L2 regularization")
        plot_dict = run_MBC_third(twenty_train, n_gram)
        plot_title = "Third configuration: Plot of F-1 score vs training data percentage"

    if mode == '4':
        print("Fourth configuration: Count Vectorizer: Remove stop words + "
              "Logistic Regression: Regularization constant, L2 regularization")
        plot_dict = run_MBC_fourth(twenty_train, n_gram)
        plot_title = "Fourth configuration: Plot of F-1 score vs training data percentage"

    if mode == '5':
        print("Fifth configuration: Count Vectorizer: Remove stop words + Logistic Regression: "
              "Regularization constant, Max iterations, L2 regularization")
        plot_dict = run_MBC_fifth(twenty_train, n_gram)
        plot_title = "Fifth configuration: Plot of F-1 score vs training data percentage"

    if mode == '6':
        print("Sixth configuration: Count Vectorizer: Remove stop words, Smooth zero counts + Logistic Regression: "
              "Regularization constant, Max iterations. L2 regularization")
        plot_dict = run_MBC_sixth(twenty_train, n_gram)
        plot_title = "Sixth configuration: Plot of F-1 score vs training data percentage"

    if mode == '7':
        print("Seventh configuration: Count Vectorizer: Smooth zero counts + Logistic Regression: "
              "Regularization constant, Max iterations, L2 regularization")
        plot_dict = run_MBC_seventh(twenty_train, n_gram)
        plot_title = "Seventh configuration: Plot of F-1 score vs training data percentage"

    if mode == '8':
        print("Eighth (best) configuration: Count Vectorizer: Smooth zero counts + Logistic Regression: "
              "Regularization constant, Max iterations, Balance class weights, L2 regularization, lbfgs solver")
        plot_dict = run_MBC_eighth(twenty_train, n_gram, '', False)
        plot_title = "Eighth (best) configuration: Plot of F-1 score vs training data percentage"

    plot_results(plot_dict, plot_title)


def display_help():
    print("[Usage]")
    print("****Best configuration mode****")
    print("python3 main.py -mbc <mode> <pkl_path> <data_path>")
    print("\n Argument details:")
    print("\n<mode>: Mode of operation. One of the following is expected:")
    print("0 : Training mode => Provide a training data set path and pkl dump path to save the trained model")
    print("1 : Test mode => Provide a pkl dumps path to load the trained model and test dataset path. "
          "The model will be loaded and test data will be classified. Classification report and plot are outputted.")
    print("\n<pkl_path>: Path for pkl files")
    print("If in training mode, this is the path to save the model pkl files")
    print("If in test mode, this is the path to load the model pkl file from")
    print("\n<data_path>: Path for data set")
    print("If in training mode, this is the path to load training data from")
    print("If in test mode, this is the path to load the test data from")
    print("\n[Examples]")
    print("To train the model:")
    print("python3 main.py -mbc 0 /Users/prasoon/Documents/books/AI/AI_Assignment_3/ "
          "/Users/prasoon/Documents/books/AI/AI_Assignment_3/dataset/Training/")
    print("To test the model:")
    print("python3 main.py -mbc 1 /Users/prasoon/Documents/books/AI/AI_Assignment_3/ "
          "/Users/prasoon/Documents/books/AI/AI_Assignment_3/dataset/Test/")
    print("\n\n****Configuration execution mode****")
    print("python3 main.py <training_data_path> <test_data_path> <config_mode> <n_gram>")
    print("\nArgument details:")
    print("\n<training_data_path>: Path for the training data set, must have a sub-directory "
          "corresponding to each class label")
    print("\n<test_data_path>: Path for the training data set, must have a sub-directory "
          "corresponding to each class label")
    print("\n<config_mode>: Configuration to determine classification mode. One of the following values is expected:")
    print("0 : Default configuration. Runs the four classifiers as base lines, plots their comparative learning curve.")
    print("1 : Best classifier configuration 1. Runs the first configuration for the best classifier, plots "
          "the learning curve for the configuration.")
    print("2 : Best classifier configuration 2. Runs the second configuration for the best classifier, plots "
          "the learning curve for the configuration.")
    print("3 : Best classifier configuration 3. Runs the third configuration for the best classifier, plots "
          "the learning curve for the configuration.")
    print("4 : Best classifier configuration 4. Runs the fourth configuration for the best classifier, plots "
          "the learning curve for the configuration.")
    print("5 : Best classifier configuration 5. Runs the fifth configuration for the best classifier, plots "
          "the learning curve for the configuration.")
    print("6 : Best classifier configuration 6. Runs the sixth configuration for the best classifier, plots "
          "the learning curve for the configuration.")
    print("7 : Best classifier configuration 7. Runs the seventh configuration for the best classifier, plots "
          "the learning curve for the configuration.")
    print("8 : Best classifier configuration 8 (the best configuration). Runs the eighth (best) configuration "
          "for the best classifier, plots the learning curve for the configuration.")
    print("\n<n_gram>: n_gram to be used for classification, 1 for uni-gram and 2 for bi-gram")
    print("\n[Examples]")
    print("To run the base line classifiers with uni-gram baseline:")
    print("python3 main.py /Users/prasoon/Documents/books/AI/dataset/Training "
          "/Users/prasoon/Documents/books/AI/dataset/Test 0 1")
    print("To run the best classifier with the best configuration with uni-gram baseline:")
    print("python3 main.py /Users/prasoon/Documents/books/AI/dataset/Training "
          "/Users/prasoon/Documents/books/AI/dataset/Test 8 1")


def display_configuration():
    print("[Parsed command line]:")
    print("Training data path: {0}".format(sys.argv[1]))
    print("Test data path: {0}".format(sys.argv[2]))
    print("Config_mode: {0}".format(sys.argv[3]))
    print("n_gram baseline mode: {0}".format(sys.argv[4]))


def main():
    if len(sys.argv) == 2 and sys.argv[1] in {"--help", "--HELP", "--h", "--H", "-h", "-H", "-help", "-HELP"}:
        display_help()
        exit(0)
    if sys.argv[1] in {"-mbc", "-MBC"}:
        print("Executing my best configuration mode..")
        if sys.argv[2] == '0' and len(sys.argv) == 5:
            print("Classifier TRAINING mode selected..")
            print("Path to save pkl file provided as: {0}".format(sys.argv[3]))
            print("Path to load training data provided as: {0}".format(sys.argv[4]))
            twenty_train = load_files(sys.argv[4], encoding='latin1')
            print("Training best configuration: Count Vectorizer: Smooth zero counts + Logistic Regression: "
                  "Regularization constant, Max iterations, Balance class weights, L2 regularization, lbfgs solver")
            run_MBC_eighth(twenty_train, '1', sys.argv[3], True)
            print("Exiting execution..")
            exit(0)
        elif sys.argv[2] == '1' and len(sys.argv) == 5:
            print("Classifier TESTING mode selected..")
            print("Path to load pkl file provided as: {0}".format(sys.argv[3]))
            print("Path to load test data provided as: {0}".format(sys.argv[4]))
            print("Executing classification on the best configuration: Count Vectorizer: Smooth zero counts + "
                  "Logistic Regression: Regularization constant, Max iterations, "
                  "Balance class weights, L2 regularization, lbfgs solver")
            test_best_configuration(sys.argv[3], sys.argv[4])
            print("Exiting execution..")
            exit(0)
        else:
            print("Invalid command line, abort!")
            display_help()
            exit(-1)

    if len(sys.argv) < 5:
        print("\n[ERROR] Incomplete arguments, abort!\n")
        display_help()
        exit(-1)
    display_configuration()
    twenty_train = load_files(sys.argv[1], encoding='latin1')

    if sys.argv[3] == '0':
        count_vect = ''
        plot_title = ''
        print("Executing default configuration for the four classifiers..")
        if sys.argv[4] == '1':
            print("n_gram configuration: Uni-gram.")
            count_vect = CountVectorizer(ngram_range=(1, 1))
            plot_title = 'Default configuration: Plot of  F-1 score vs training data percentage (Uni-gram case)'
        elif sys.argv[4] == '2':
            print("n_gram configuration: Bi-gram.")
            count_vect = CountVectorizer(ngram_range=(2, 2))
            plot_title = 'Default configuration: Plot of  F-1 score vs training data percentage (Bi-gram case)'
        else:
            print("Invalid n_gram configuration, must be 1 or 2, abort!")
            display_help()
            exit(-1)
        plot_dict = run_default_classification(twenty_train, count_vect)
        plot_results(plot_dict, plot_title)
    elif sys.argv[3] in ['1', '2', '3', '4', '5', '6', '7', '8']:
        if sys.argv[4] == '1' or sys.argv[4] == '2':
            my_best_configuration(twenty_train, sys.argv[3], sys.argv[4])
        else:
            print("Invalid n_gram configuration, must be 1 or 2, abort!")
            display_help()
            exit(-1)
    else:
        print("Invalid config_mode supplied, must be between 0 and 8 inclusive, abort!")
        display_help()
        exit(-1)


if __name__ == '__main__':
    main()
    print("End of execution..")
